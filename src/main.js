import Vue from 'vue';
import App from './App.vue';
import './App.scss';

const app = new Vue({
  el: '#app',
  render: h => h(App)
});